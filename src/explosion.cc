#include "explosion.hh"
#include "renderer.hh"
#include "utility.hh"
#include <iostream>
#include <algorithm>


const unsigned int Explosion::PARTICLE_COUNT = 500;
const int Explosion::COLOR_STEP = 1;
const float Explosion::SPEED_SCALE = 1.0f;  // 0.1f
const float Explosion::MAX_SPEED = 50;
const bool Explosion::BLUR_ENABLED = false;
const float Explosion::ALPHA_STEP = 1.0f;

std::unordered_map<unsigned int, unsigned int> Explosion::MASK_MAP = {

    {0xFF000000, 24},  // red
    {0x00FF0000, 16},  // green
    {0x0000FF00,  8}   // blue    
    
    
};



Explosion::Explosion(const SDL_Rect &dim, const unsigned int bgColor):
    mDim(dim),
    mBgColor(bgColor),
    mNewBegin(0),
    mAlpha(255.0f),
    mLoops(0),
    mFinished(false) {
        
        // unsigned int tBegin = SDL_GetTicks();
        
        init();
        
        // unsigned int tEnd = SDL_GetTicks();
        // std::cout << "init time: " << tEnd - tBegin << " ms\n";
    
}


Explosion::~Explosion() {
    
    SDL_DestroyTexture(mTex);
    
    delete[] mParticles;
    delete[] mPixels1;  // FIXED?: causes "double free or corruption (!prev)"
    // delete[] mPixels2;
}


void Explosion::update(SDL_Renderer* r) {
    
    if (mFinished) return;
    
    // update particle attributes
    for (unsigned int i=0; i<PARTICLE_COUNT; ++i) {
        mParticles[i].pos.x += mParticles[i].vel.x * SPEED_SCALE;
        mParticles[i].pos.y += mParticles[i].vel.y * SPEED_SCALE;
        
        mParticles[i].color.a = mAlpha;
    }
    
    // frame based fade
    mAlpha -= ALPHA_STEP;
    if (mAlpha <= 0.0f) {
        mAlpha = 0.0f;
        mFinished = true;
    } 
    
    // lock entire texture
    void** ptr = reinterpret_cast<void**> (&mPixels1);
    SDL_LockTexture(mTex, nullptr, ptr, &mPitch);
    
    // clearTexture(BLUR_ENABLED);
    if (!BLUR_ENABLED) clearBuffer(mPixels1);
    
    
    // render particles
    for (unsigned int i=mNewBegin; i<PARTICLE_COUNT; ++i) {
        float x = mParticles[i].pos.x;
        float y = mParticles[i].pos.y;
        
        float maxDist = 0.0f;
        if (mDim.w < mDim.h) maxDist = mDim.w/2.0f;
        else maxDist = mDim.h/2.0f;
        
        float d = mParticles[i].pos.distance({mDim.w/2.0f, mDim.h/2.0f});
        if ( d < maxDist-5.0f) {
            setPixel(x, y, mParticles[i].color, mPitch);
        
        } else {
            ++mNewBegin;
        }
        
         
    }
    
    if (BLUR_ENABLED) simpleBoxBlur();
    
    // apply changes
    SDL_UnlockTexture(mTex);
    SDL_RenderCopy(r, mTex, nullptr, &mDim);
    
    
}


void Explosion::setPixel(int x, int y, const SDL_Color &color, int pitch) {
    
    unsigned int offset = y * pitch / sizeof(unsigned int);
    mPixels1[x + offset] = evaluateColor(color);
}


void Explosion::setPixel(int x, int y, unsigned int color, int pitch) {
    
    unsigned int offset = y * pitch / sizeof(unsigned int);
    mPixels1[x + offset] = color; 
}


unsigned int Explosion::getPixel(int x, int y, int pitch) {
    
    unsigned int offset = y * pitch / sizeof(unsigned int);
    return mPixels1[x + offset];
}


void Explosion::init() {

    mTex = SDL_CreateTexture(Renderer::getInstance()->getRenderer(), 
                             SDL_PIXELFORMAT_RGBA8888, 
                             SDL_TEXTUREACCESS_STREAMING, 
                             mDim.w, 
                             mDim.h);
                                           
                           
    // only for no blur, TODO: add add to particle pixels too to solve? 
    if (!BLUR_ENABLED) {
        SDL_SetTextureBlendMode(mTex, SDL_BLENDMODE_BLEND);  
     
    // for black bg with blur
    } else if (mBgColor == 0 && BLUR_ENABLED) {
        SDL_SetTextureBlendMode(mTex, SDL_BLENDMODE_ADD); 
    }
    // for white bg with blur 
    // else if (mBgColor == 0xFFFFFF00) SDL_SetTextureBlendMode(mTex, SDL_BLENDMODE_MOD);
    
    
    // texture width * colorbytes (format RGBA8888 -> 32bit == 4 bytes)
    mPitch = mDim.w * 4;
    
    mPixels1 = new unsigned int[mDim.w*mDim.h];
    // if (BLUR_ENABLED) mPixels2 = new unsigned int[mDim.w*mDim.h]; 
    
    mParticles = new Particle[PARTICLE_COUNT];
    
    for (unsigned int i=0; i<PARTICLE_COUNT; ++i) {
        
        float angle = Utility::random(0, 3600) / 10.0f;
        Vec2 vel = Vec2::rotateVector( {0.0f, -1.0f}, angle );
        
        float magnitude = Utility::random(0, 250) / 100.0f;
        magnitude *= magnitude;  // exaggarate magnitude differences
        
        // limit speed
        // if (magnitude > MAX_SPEED) magnitude = MAX_SPEED;
        
        vel.x *= magnitude;
        vel.y *= magnitude;
        
        mParticles[i] = Particle{ {mDim.w/2.0f, mDim.h/2.0f}, vel, {255, 255, 255, 255} };
        
        velocityBasedColor(i);
    }
    
    
    // NOTE: only for velocity based coloring
    // NOTE: no need for elements to be ptrs to Particles since sorting happens only once
    // since vel is constant, amount of particles to be updated can be diminished
    // each loop when a particle is out of view
    std::sort( mParticles, mParticles+PARTICLE_COUNT, 
               [](const Particle &a, const Particle &b) {
                   return a.vel.magnitude() > b.vel.magnitude();     
               } );
    
    
    void** ptr = reinterpret_cast<void**> (&mPixels1);
    SDL_LockTexture(mTex, nullptr, ptr, &mPitch);
    
    clearBuffer(mPixels1);
    
    SDL_UnlockTexture(mTex);
    
    mDim.x -= mDim.w/2;
    mDim.y -= mDim.h/2;
    

}


void Explosion::clearBuffer(unsigned int* buffer) {
    
    unsigned int offset = 0;
    for (int i=0; i<mDim.w; ++i) {
        for (int j=0; j<mDim.h; ++j) {
                offset = i * mPitch / sizeof(unsigned int);
                buffer[j + offset] = mBgColor;     
        }
    }
}


unsigned int Explosion::evaluateColor(const SDL_Color &color) {
    
    unsigned int r = color.r * 0x1000000;  // 0xFF000000
    unsigned int g = color.g * 0x10000;    // 0x00FF0000
    unsigned int b = color.b * 0x100;      // 0x0000FF00
    
    return (r + g + b + color.a);
    
}


void Explosion::frameBasedColor(unsigned int index) {
    
    if (mParticles[index].color.g - COLOR_STEP >= 0 && mParticles[index].color.b == 0) {
        mParticles[index].color.g -= COLOR_STEP;
    }
    
    if (mParticles[index].color.b - 2*COLOR_STEP < 0) mParticles[index].color.b = 0;
    else mParticles[index].color.b -= 2*COLOR_STEP;
    
}


void Explosion::velocityBasedColor(unsigned int index) {
    
          
    float mag = mParticles[index].vel.magnitude();
    
    if (mag == 0.0f) return;
    
    mParticles[index].color.r = 255;
    mParticles[index].color.g = 255 / mag;
    mParticles[index].color.b = 255 / (mag*mag);
}


bool Explosion::isFinished() {
    return mFinished;
}

void Explosion::fastBoxBlur() {
    
    // swap buffers
    unsigned int* tmp = mPixels1;
    mPixels1 = mPixels2;
    mPixels2 = tmp;
    
    unsigned int redMask = 0xFF000000;
    unsigned int greenMask = 0x00FF0000;
    unsigned int blueMask = 0x0000FF00;
    
    float xEdge = mDim.w;
    float yEdge = mDim.h;
    
    float xCenter = mDim.w/2;
    float yCenter = mDim.h/2;
    
    float r = sqrt( (xEdge-xCenter) * (xEdge-xCenter) + (yEdge-yCenter) * (yEdge-yCenter) );
    

    // TODO: possibly made originally with matlab: https://www.peterkovesi.com/matlabfns/
    // -> INDEX STARTS AT 1
    // TOOD: remove mem leaks
    // TODO: add mRedTotal, mGreenTotal and mBlueTotal to store accum, and set pixels
    // on every iteration -> no separate color component passes 
    
    gaussBlur_4 (mPixels1, mPixels2, mDim.w, mDim.h, r, redMask);
    gaussBlur_4 (mPixels1, mPixels2, mDim.w, mDim.h, r, greenMask);
    gaussBlur_4 (mPixels1, mPixels2, mDim.w, mDim.h, r, blueMask);
    
    
}


void Explosion::simpleBoxBlur() {
    
    for (int y=0; y<mDim.h; ++y) {
        
        // if (y < 0 || y > Renderer::W_HEIGHT) continue;
        
        for (int x=0; x<mDim.w; ++x) {
            
            // if (x < 0 || x > Renderer::W_WIDTH) continue;
            
            
            int redTotal = 0;
            int greenTotal = 0;
            int blueTotal = 0;
            
            for (int j=-1; j<=1; ++j) {
                for (int i=-1; i<=1; ++i) {
                    
                    int currentX = x + i;
                    int currentY = y + j;
                    
                    if (currentX >= 0 && currentX < mDim.w &&
                        currentY >= 0 && currentY < mDim.h) {
                            
                        uint32_t color = mPixels1[currentY*mDim.w + currentX]; 
                        
                        redTotal += (color & 0xFF000000) >> 24;                           
                        greenTotal += (color & 0x00FF0000) >> 16;
                        blueTotal += (color & 0x0000FF00) >> 8;
                        
                    }
                    
                }
            }
            
            
            
            // divide by 9, since the pixel to be colored is the 9th
            // and the avg of pixels surrounding it
            uint8_t red   = redTotal / 9;
            uint8_t green = greenTotal / 9;
            uint8_t blue  = blueTotal / 9;
            uint8_t alpha = 255;
            
            // std::cout << (unsigned int)alpha << "\n";
            
            
            float dist = sqrt( (x-mDim.w/2) * (x-mDim.w/2) + (y-mDim.h/2) * (y-mDim.h/2) );
            uint8_t step = 10;
            float lowerLimit = 30.0f;
            float upperLimit = 60.0f;
        
            if (dist > lowerLimit && alpha >= step) {
                alpha -= step;
                
            } else if (dist > lowerLimit && alpha < step) {
                alpha = 0;
                
            } else if (dist > upperLimit && alpha >= 2 * step) {
                alpha -= 2 * step;
            
            } else if (dist > upperLimit && alpha < 2 * step) {
                alpha = 0;
            }
            
            
            
            // float factor = alpha / 255;
            
            // if (red > factor)   red   -= factor;
            // if (green > factor) green -= factor;
            // if (blue > factor)  blue  -= factor;
            
            
            
            // TODO: add threshold to decr alpha on pixels with slight blend color too
            if ( red == 0 && green == 0 && blue == 0) alpha = 0;             
            setPixel(x, y, {red, green, blue, alpha}, mPitch);
            
            
            
        }
        
    }
    
    
        
}


int* Explosion::initGaussiaBoxes(float sigma, int n) {
    
    // Ideal averaging filter width
    int wl = sqrt( (12*sigma*sigma/n) + 1 );  
    
    if(wl % 2 == 0) wl--;
    int wu = wl + 2;
				
    int mIdeal = (12*sigma*sigma - n*wl*wl - 4*n*wl - 3*n) / (-4*wl - 4);
    int m = round(mIdeal);
    // int sigmaActual = Math.sqrt( (m*wl*wl + (n-m)*wu*wu - n)/12 );
				
    int* boxes = new int[n];  
    for (int i=0; i<n; i++) {
        
        // boxes[i] = i < m ? wl:wu;
        if (i < m) boxes[i] = wl;
        else boxes[i] = wu;
        
    }
    
    return boxes;
}


void Explosion::gaussBlur_4 (unsigned int* srcBuffer, unsigned int* dstBuffer, int w, int h, float r, unsigned int colormask) {
    int* boxes = initGaussiaBoxes(r, 3);
    boxBlur_4 (srcBuffer, dstBuffer, w, h, (boxes[0]-1) / 2, colormask);
    boxBlur_4 (dstBuffer, srcBuffer, w, h, (boxes[1]-1) / 2, colormask);
    boxBlur_4 (srcBuffer, dstBuffer, w, h, (boxes[2]-1) / 2, colormask);
    
    delete[] boxes;
}


void Explosion::boxBlur_4 (unsigned int* srcBuffer, unsigned int* dstBuffer, int w, int h, float r, unsigned int colormask) {
    
    // copy source to destination buffer
    for(int i=0; i<mDim.w*mDim.h; i++) {
        
        // read only one color component
        // NOTE: must use map::at since [] operator disqualifies const
        dstBuffer[i] = (srcBuffer[i] & colormask) >> MASK_MAP[colormask];
    }    
    
    boxBlurH_4(dstBuffer, srcBuffer, w, h, r, colormask);
    boxBlurT_4(srcBuffer, dstBuffer, w, h, r, colormask);
}



void Explosion::boxBlurH_4 (unsigned int* srcBuffer, unsigned int* dstBuffer, int w, int h, float r, unsigned int colormask) {
    
    int iarr = 1 / (r+r+1);
    
    for(int i=0; i<h; i++) {
        
        int ti = i*w;
        int li = ti; 
        int ri = ti+r;
        
        int fv = (srcBuffer[ti] & colormask) >> MASK_MAP[colormask]; 
        int lv = (srcBuffer[ti+w-1] & colormask) >> MASK_MAP[colormask];
        int val = (r+1)*fv;
        
        for(int j=0; j<r; j++) {
            val += ( (srcBuffer[ti+j] & colormask) >> MASK_MAP[colormask] );
        }
        
        for(int j=0; j<=r; j++) { 
            val += ( (srcBuffer[ri++] & colormask) >> MASK_MAP[colormask] ) - fv;   
            dstBuffer[ti++] = round(val*iarr); 
        }
        
        for(int j=r+1; j<w-r; j++) { 
            val += ( (srcBuffer[ri++] & colormask) >> MASK_MAP[colormask] ) - ( (srcBuffer[li++] & colormask) >> MASK_MAP[colormask] );   
            dstBuffer[ti++] = round(val*iarr); 
        }
        
        for(int j=w-r; j<w; j++) { 
            val += lv - ( (srcBuffer[li++] & colormask) >> MASK_MAP[colormask] );
            dstBuffer[ti++] = round(val*iarr); 
        }
        
    }
}


void Explosion::boxBlurT_4 (unsigned int* srcBuffer, unsigned int* dstBuffer, int w, int h, float r, unsigned int colormask) {
    
    int iarr = 1 / (r+r+1);
    
    for (int i=0; i<w; i++) {
        
        int ti = i;
        int li = ti;
        int ri = ti + r * w;
        
        int fv = (srcBuffer[ti] & colormask) >> MASK_MAP[colormask];
        int lv = (srcBuffer[ti + w * (h-1)] & colormask) >> MASK_MAP[colormask];
        int val = (r+1)*fv;
        
        for(int j=0; j<r; j++) {
            val += ( (srcBuffer[ti+j*w] & colormask) >> MASK_MAP[colormask] );
        }
                
        for(int j=0; j<=r; j++) { 
            val += ( (srcBuffer[ri] & colormask) >> MASK_MAP[colormask] ) - fv;  
            dstBuffer[ti] = round(val*iarr);
            ri += w; 
            ti += w; 
        }
        
        for(int j=r+1; j<h-r; j++) { 
            val += ( (srcBuffer[ri] & colormask) >> MASK_MAP[colormask] ) - ( (srcBuffer[li] & colormask) >> MASK_MAP[colormask] );
            dstBuffer[ti] = round(val*iarr);
            li += w;
            ri += w;
            ti += w;
        }
        
        for(int j=h-r; j<h; j++) { 
            val += lv - ( (srcBuffer[li] & colormask) >> MASK_MAP[colormask] );
            dstBuffer[ti] = round(val*iarr);
            li += w;
            ti += w;
        }
        
    }
}