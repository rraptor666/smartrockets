#ifndef EXPLOSION_HH
#define EXPLOSION_HH

#include "vec2.hh"
#include <SDL2/SDL.h>
#include <vector>
#include <random>
#include <unordered_map>


struct Particle {
    Vec2 pos;         // position  
    Vec2 vel;         // 2D velocity
    SDL_Color color;  // pixel color
};



class Explosion {

public:
    Explosion(const SDL_Rect &dim, const unsigned int bgColor=0x000000FF);
    ~Explosion();
    
    /**
     * @brief update, updates particles in the texture
     * 
     * @param r, ptr to SDL_Renderer object 
     */
    void update(SDL_Renderer* r);
    
    
    /**
     * @brief setPixel, colors the user defined pixel
     * 
     * @param x, x coordinate of pixel in texture 
     * @param y, y coordinate of pixel in texture
     * @param color, color of the pixel as {r, g, b, a}
     * @param pitch, row width of pixel in texture in bytes 
     */
    void setPixel(int x, int y, const SDL_Color &color, int pitch);
    
    
    /**
     * @brief getPixel, get color of requested pixel
     * 
     * @param x, x coordinate of pixel in texture 
     * @param y, y coordinate of pixel in texture 
     * @param pitch, row width of pixel in texture in bytes 
     * @return unsigned int, color value in form 0xRRGGBBAA (white = 0xFFFFFFFF) 
     */
    unsigned int getPixel(int x, int y, int pitch);
    
    
    /**
     * @brief setPixel, colors the user defined pixel
     * 
     * @param x, x coordinate of pixel in texture 
     * @param y, y coordinate of pixel in texture
     * @param color, color of the pixel as RRGGBBAA (hex)
     * @param pitch, row width of pixel in texture in bytes 
     */
    void setPixel(int x, int y, unsigned int color, int pitch);
    
    
    /**
     * @brief isFinished, true if animation is over, otherwise false 
     */
    bool isFinished();
    

private:
    void init();

    /**
     * @brief clearBuffer, colors pixels of buffer with background color "mBgColor"
     */
    void clearBuffer(unsigned int* buffer);
    
    
    /**
     * @brief evaluateColor, converts SDL_Color {r, g, b, a} into unsigned int 
     *        0xRRGGBBAA
     */
    unsigned int evaluateColor(const SDL_Color &color);

    
    /**
     * @brief frameBasedColor, colors pixel based on frames passed since first 
     *        update frame
     * 
     * @param index, index of pixel in Particle array "mParticles"
     */
    void frameBasedColor(unsigned int index);
    
    
    
    /**
     * @brief velocityBasedColor, colors pixel based on magnitude of velocity
     *        vector of Particle
     * 
     * @param index, index of pixel in Particle array "mParticles"
     */
    void velocityBasedColor(unsigned int index);
    
    
    /**
     * @brief fastBoxBlur, box blur with optimized algorithm
     */
    void fastBoxBlur();
    
    
    /**
     * @brief simpleBoxBlur, blur by calculating pixel's color by taking avg color 
     *        of the pixels surrounding it
     */
    void simpleBoxBlur();
    
    
    /**
     * @brief initGaussianBoxes, creates boxes for fastBoxBlur
     * 
     * @param sigma, standard deviation 
     * @param n, number of boxes 
     * @return int*, ptr to box array 
     */
    int* initGaussiaBoxes(float sigma, int n);

    
    // servant functions for fastBoxBlur
    void gaussBlur_4(unsigned int* scl, unsigned int* tcl, int w, int h, float r, unsigned int colormask);
    void boxBlur_4(  unsigned int* scl, unsigned int* tcl, int w, int h, float r, unsigned int colormask);
    void boxBlurH_4( unsigned int* scl, unsigned int* tcl, int w, int h, float r, unsigned int colormask);
    void boxBlurT_4( unsigned int* scl, unsigned int* tcl, int w, int h, float r, unsigned int colormask);

    SDL_Rect mDim;
    SDL_Texture* mTex;

    unsigned int* mPixels1;
    unsigned int* mPixels2;  // addditional buffer for fastBoxBlur
    
    Particle* mParticles;
    
    int mPitch;  // width of pixel row
    
    unsigned int mBgColor;   // background color of texture  
    unsigned int mNewBegin;  // optimization for number of updatable particles
    
    float mAlpha;  // global alpha value for more precise changes
    unsigned int mLoops;
    
    bool mFinished;  // whether animation is over and update should be halted
    

    static const unsigned int PARTICLE_COUNT; 
    static const int COLOR_STEP;
    static const float SPEED_SCALE;
    static const float MAX_SPEED;
    static const bool BLUR_ENABLED;
    static const float ALPHA_STEP;
    
    static std::unordered_map<unsigned int, unsigned int> MASK_MAP;
    
};


#endif