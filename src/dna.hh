#ifndef DNA_HH
#define DNA_HH

#include "vec2.hh"
#include "utility.hh"
#include <vector>

using ForceVec = std::vector<Vec2>;


/**
 * @brief Dna, class for representing attributes which offspring will inherit
 *        from its parents
 */
class Dna {
    
public:
    Dna(unsigned int amount);
    Dna(const Dna &other);
    Dna(ForceVec &genes);
    
    ~Dna();
    
    
    /**
     * @brief reset, clear and recreate genes
     */
    void reset();
    
    
    /**
     * @brief create, creates new genes, a container filled with force vectors pointing
     *        in random directons  
     * 
     * @param amount, number of genes to add to the container
     */
    void create(unsigned int amount);
    
    
    /**
     * @brief getGenes, return pointer to gene container
     */
    const ForceVec* getGenes();
    
    
    /**
     * @brief mutation, inflick mutation (slight change) with certain probability
     */
    void mutation();
    
    
    /**
     * @brief crossover, create new offspring's genes by crossovering parents' genes
     * 
     * @param partner, the other parent
     * @return Dna, dna of the new offspring
     */
    Dna crossover(const Dna &partner);
    


private:
    
    ForceVec mGenes;
    
    
    static const float MUTATION_RATE;
    static const float MAX_FORCE;
    
    
};



#endif