#include "engine.hh"

Engine::Engine(TexVec* tex):
    mTextures(tex),
    mFrame(0),
    mAngle(0.0f) {
    
}


Engine::~Engine() {
    
}


void Engine::update(SDL_Renderer* r, 
                          const SDL_Point& center, 
                          const SDL_Rect &pos,
                          float angle) {
                    
    SDL_Texture* tex = mTextures->at(mFrame);
    SDL_RenderCopyEx(r, tex, nullptr, &pos, angle, &center, SDL_FLIP_NONE);   
    
    ++mFrame;
    if (mFrame >= mTextures->size()) mFrame = 0;

}
