#include "maincontrol.hh"
#include <iostream>


int main(int argc, char** argv) {
    
    srand( time(0) );

    MainControl mc;                     
    
    if ( mc.isInited() ) {
        mc.run();
        
    } else {
        std::cerr << "error\n";
        return 1;
    }
    
    std::cerr << "exited normally\n";
    return 0;
}


