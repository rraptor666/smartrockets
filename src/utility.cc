#include "utility.hh"
#include <math.h>


SDL_Texture* Utility::loadTexture(SDL_Renderer* r, const std::string &path) {
    
    SDL_Surface* surface = IMG_Load( path.c_str() );
    
    if (surface == nullptr) {
        std::cerr << IMG_GetError() << "\n";
        return nullptr;
    }
    
    SDL_Texture* tex = SDL_CreateTextureFromSurface(r, surface);
    SDL_FreeSurface(surface);
    
    if (tex == nullptr) {
        std::cerr << SDL_GetError() << "\n";
        return nullptr;
    }

    return tex; 
    
}




float Utility::map(const float value,
          const float min1, const float max1,
          const float min2, const float max2) {

    return min2 + ((max2 - min2) / (max1 - min1)) * (value - min1);
}


int Utility::random(const int &min, const int &max) {
    return  min + rand() % (max-min+1);
}


float Utility::cos(float f) {
    float val = std::cos(f);
    if ( isnan(val) ) val = 0.0f;
    
    return val * M_PI;
}


float Utility::sin(float f) {
    float val = std::sin(f);
    if ( isnan(val) ) val = 0.0f;
    
    return val * M_PI;
}


bool Utility::outOfView(const SDL_Rect* rect, int w, int h) {
    SDL_Rect window = {0, 0, w, h};
    return !SDL_HasIntersection(rect, &window);
}


bool Utility::outOfView(const SDL_Point* point, int w, int h) {
    SDL_Rect window = {0, 0, w, h};
    return !SDL_PointInRect(point, &window);
}