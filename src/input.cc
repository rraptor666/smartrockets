#include "input.hh"


const float Input::ENTER_DELAY = 0.3f;  // seconds

Input* Input::getInstance() {
    static Input instance;
    return &instance;
}


Input::Input():
    mPrevPress(0) {
    
}


bool Input::shouldQuit() {
    
    while ( SDL_PollEvent(&mEvent) ) {
        
        if (mEvent.type == SDL_QUIT || mEvent.key.keysym.sym == SDLK_q ) {
            return true;
        }
    }
    
    return false;
    
}


bool Input::enterPressed() {
    
    unsigned int currentTime = SDL_GetTicks();
    
    // button debounce
    float elapsedSeconds = (currentTime - mPrevPress) / 1000.0f;
    if (elapsedSeconds < ENTER_DELAY) return false; 
        
    SDL_PumpEvents();
    const uint8_t* states = SDL_GetKeyboardState(NULL);
    
    if ( states[SDL_SCANCODE_RETURN] ) {
        mPrevPress = currentTime;
        return true;
    } 
    
    return false;
}


    
    