#ifndef MAINCONTROL_HH
#define MAINCONTROL_HH

#include "input.hh"
#include "population.hh"
#include "renderer.hh"
#include "explosion.hh"

#include <vector>


/**
 * @brief MainControl, class for controlling flow of the program, contains the main
 *        main update loop
 * 
 */
class MainControl {
    
public:
    
    MainControl();
    ~MainControl();
    
    /**
     * @brief run, start the update loop
     */
    void run();
    bool isInited();    
    
    void newExplosion(int x, int y);


private:
    bool init();
    void handleExplosions();

    bool mInited;
    
    Renderer* mRenderer;
    Input* mInput;
    
    Population mPopulation;
    
    std::vector<Explosion*> mExplosions;
    unsigned int mExplosionColor;
    
    
};


#endif