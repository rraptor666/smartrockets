#ifndef INPUT_HH
#define INPUT_HH


#include <SDL2/SDL.h>


class Input {

public:
    static Input* getInstance();
    bool shouldQuit();
    bool enterPressed();


private:
    Input();
    ~Input() {};
    
    unsigned int mPrevPress;
    SDL_Event mEvent;
    
    static const float ENTER_DELAY;
    
};


#endif
