#include "rocket.hh"
#include "utility.hh"

const Vec2 Rocket::UP = {0.0f, -1.0f};
const float Rocket::FORCE_FACTOR = 0.1f;
const float SCALE = 1.0f;
const float ENGINE_W = 30.0f * SCALE;
const float ENGINE_H = 42.0f * SCALE;
const float OFFSET = -10.0f * SCALE;

Rocket::Rocket(const SDL_Rect &pos, 
               SDL_Texture* rockettex, 
               unsigned int lifespan, 
               TexVec* enginetex):
    mPos(pos),
    mStartPos( {pos.x, pos.y} ),
    mTex(rockettex),
    mEngine(enginetex),
    mFrame(0),
    mMaxFrames(lifespan),
    mDestroyed(false),
    mFinished(false),
    mDna(lifespan),
    mFitness(0.0f) {

        int w = ENGINE_W;
        int h = ENGINE_H;
        mEnginePos = {pos.x, (int)(pos.y + pos.h + OFFSET), w, h};
        mEngineStartPos = mEnginePos;
            
        reset();
}


Rocket::Rocket(const SDL_Rect &pos, 
               SDL_Texture* rockettex, 
               unsigned int lifespan, 
               const Dna &dna,
               TexVec* enginetex):
    mPos(pos),
    mStartPos( {pos.x, pos.y} ),
    mTex(rockettex),
    mEngine(enginetex),
    mFrame(0),
    mMaxFrames(lifespan),
    mDestroyed(false),
    mFinished(false),
    mDna(dna),
    mFitness(0.0f) {
    
        int w = ENGINE_W;
        int h = ENGINE_H;
        mEnginePos = {pos.x, (int)(pos.y + pos.h + OFFSET), w, h};
        mEngineStartPos = mEnginePos;
    
        reset();
}

Rocket::~Rocket() {
    
}


void Rocket::update(SDL_Renderer* r) {
    
    if (!mFinished || !mDestroyed) {
        ++mFrame;
    }
    
    if (mFrame >= mMaxFrames) {
        mFrame = mMaxFrames - 1;
        return;
    }   
    
    if (!mFinished) {
        move();
    }
    
    
    // body
    SDL_Point nozzle = {mEnginePos.w/2, (int)(-mPos.h/2 - OFFSET)};
    SDL_Point center = {mPos.w/2, mPos.h/2};
    
    // TODO: make more genenic using scale?
    // SDL_Point center = {mPos.w/2, mPos.y /* + mPos.h/SCALE */ - mEnginePos.y}; 
    // SDL_Point center = {mPos.w/2, mPos.h + mEnginePos.h};
    
    mEngine.update(r, nozzle, mEnginePos, mAngle);
    SDL_RenderCopyEx(r, mTex, nullptr, &mPos, mAngle, &center, SDL_FLIP_NONE);
    
    // TODO: refactor hard coded target out
    SDL_Rect target = {400, 40, 20, 20};
    if ( SDL_HasIntersection(&mTipPos, &target) ) mFinished = true;
    
    
}


void Rocket::reset(bool hardreset) {
    mAccel = 0.0f;
    mVel = 0.0f;
    mAccum = 0.0f;
    
    mFitness = 0.0f;
    mDestroyed = false;
    mFinished = false;
    mFrame = 0;
    
    mPos.x = mStartPos.x;
    mPos.y = mStartPos.y;
    
    mTipPos = {mPos.x+5, mPos.y, mPos.w-5, mPos.w-5};
    
    mEnginePos.x = mEngineStartPos.x;
    mEnginePos.y = mEngineStartPos.y;
    
    if (hardreset) mDna.reset();
    
}


void Rocket::applyForce(const float f) {
    mAccel += f;
}


void Rocket::applyForce(const Vec2 &v) {
    mAccel += v * FORCE_FACTOR;
}


const SDL_Rect* Rocket::getPos() {
    return &mPos;
}


const SDL_Point Rocket::getTipPos() {
    return {mTipPos.x + mTipPos.w/2, mTipPos.y + mTipPos.h/2};
}

bool Rocket::isDestroyed() {
    return mDestroyed;
}


bool Rocket::crash() {
    bool prev = mDestroyed;
    mDestroyed = true;
    return prev;
}


float Rocket::getFitness(const SDL_Point &target) {
    
    float x = target.x - mPos.x + mPos.w/2;
    float y = target.y - mPos.y + mPos.h/2;
    float d = sqrt(x*x + y*y);
    mFitness = 1000 / (d*d) * mMaxFrames/pow(mFrame, 1);  
    
    if (mDestroyed) mFitness = 0.0f;
    if (mFinished)  mFitness *= 10.0f;
    
    
    return mFitness;
}


void Rocket::normalizeFitness(float maxFit) {
    mFitness /= maxFit;
}


Dna* Rocket::getDna() {
    return &mDna;
}



void Rocket::move() {

    applyForce( mDna.getGenes()->at(mFrame) );
    
    mVel += mAccel;
    mAccel = 0.0f;

    mAccum.x += mVel.x;
    mAccum.y += mVel.y;
    
    float deltaX = mAccum.x + mStartPos.x;
    float deltaY = mAccum.y + mStartPos.y;
    
    mPos.x = deltaX;
    mPos.y = deltaY;
    
    mTipPos.x = deltaX;
    mTipPos.y = deltaY;
    
    mEnginePos.x = mAccum.x + mEngineStartPos.x;
    mEnginePos.y = mAccum.y + mEngineStartPos.y;
    
    mAngle = mAccum.vectorAngle(UP);
    if (mAccum.x < 0.0f) mAngle = -mAngle;
        
}