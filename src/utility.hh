#ifndef UTILITY_HH
#define UTILITY_HH


#include <iostream>
#include <random>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>


class Utility {
    
public:

    static SDL_Texture* loadTexture(SDL_Renderer* r, const std::string &path);

    static float map(const float value,
              const float min1, const float max1,
              const float min2, const float max2);


    static int random(const int &min, const int &max);


    // [-PI, PI]
    static float cos(float f);
    static float sin(float f);
    
    static bool outOfView(const SDL_Rect* rect, int w, int h);
    static bool outOfView(const SDL_Point* point, int w, int h);

};





#endif
