#include "population.hh"
#include "renderer.hh"
#include "utility.hh"
#include "maincontrol.hh"
#include <iostream>
#include <iomanip>


const unsigned int Population::ROCKET_COUNT = 10;
const unsigned int Population::LIFESPAN = 300;


Population::Population():
    mCount(0),
    mRocketFactory( RocketFactory::getInstance() ) {
        
    
    mRocketFactory->initialize(Renderer::W_WIDTH, Renderer::W_HEIGHT);
    mTarget = {Renderer::W_WIDTH/2, 40, 20, 20};
    
    for (unsigned int i=0; i<ROCKET_COUNT; ++i) {
        mRockets.push_back( mRocketFactory->buildRocket(LIFESPAN) );
                                    
    }
    
}


Population::~Population() {
    
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        delete mRockets[i];
    }
}




void Population::reset() {
    
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        mRockets[i]->reset(true);
    }
    mMatingPool.clear();
    mCount = 0;
    
}
    
    
void Population::update(MainControl* obj) {
    
    SDL_Renderer* r = Renderer::getInstance()->getRenderer();
    for (auto it = mRockets.begin(); it != mRockets.end(); ++it) {
        
        SDL_Point tip = (*it)->getTipPos();
        if ( Utility::outOfView(&tip, Renderer::W_WIDTH, Renderer::W_HEIGHT) ) {
            
            if ( !(*it)->crash() ) obj->newExplosion(tip.x, tip.y);
        }
        
        (*it)->update(r);
    }
    
    SDL_SetRenderDrawColor(r, 255, 0, 0, 255);
    SDL_RenderDrawRect(r, &mTarget);
    SDL_RenderFillRect(r, &mTarget);
    
    
    ++mCount;
    if (mCount >= LIFESPAN) {
        evaluate( {mTarget.x + mTarget.w/2, mTarget.y + mTarget.h/2} );
        selection();
        mCount = 0;
        
    }
    

}


void Population::evaluate(const SDL_Point &target) {
    
    mMatingPool.clear();
    
    float maxFitness = 0.0f;
    float minFitness = std::numeric_limits<float>::max();
    float avgFitness = 0.0f;
    
    // find max fitness
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        
        float fitness = mRockets[i]->getFitness(target);
        
        if (fitness > maxFitness) maxFitness = fitness;
        if (fitness < minFitness) minFitness = fitness;
        avgFitness += fitness;
        
    }
    avgFitness /= mRockets.size();
    
    // normalize fitnesses
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        mRockets[i]->normalizeFitness(maxFitness);
    } 
    
    
    std::cout << "Count: " << mRockets.size()
              << std::fixed << std::setprecision(3)
              << ", Min: " << minFitness
              << ", Max: " << maxFitness
              << ", Avg: " << avgFitness
              << "\n";
    
    
    // TODO: replace with system of more prob of picking certain index
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        unsigned int n = mRockets[i]->getFitness(target) * 100.0f;
        
        for (unsigned int j=0; j<n; ++j) {
            mMatingPool.push_back( mRockets[i] );   
        }
        
    }
    
    
}


void Population::selection() {
    
    std::vector<Rocket*> newGeneration;
    for (unsigned int i=0; i<mRockets.size(); ++i) {
        
        if ( mMatingPool.empty() ) {
            std::cout << "mating pool empty!\n";
            
            for (auto rocket : mRockets) {
                rocket->reset();
            }
            return;    
        }      
        unsigned int indexA = Utility::random(0, mMatingPool.size()-1);
        unsigned int indexB = Utility::random(0, mMatingPool.size()-1);
        
        Dna* parentA = mMatingPool[indexA]->getDna();
        Dna* parentB = mMatingPool[indexB]->getDna(); 
        Dna child = parentA->crossover(*parentB);
        
        child.mutation();
        
        newGeneration.push_back( mRocketFactory->buildRocket(LIFESPAN, child) );
        
    }
    
    // clear
    for (auto it = mRockets.begin(); it != mRockets.end();) {
        delete *it;
        it = mRockets.erase(it);
    }
    
    mRockets = newGeneration;
    
    
    
    
}


unsigned int Population::getCurrentFrame() {
    return mCount;
}