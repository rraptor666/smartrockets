#include "maincontrol.hh"
#include "utility.hh"


MainControl::MainControl():
    mRenderer( Renderer::getInstance() ),
    mInput( Input::getInstance() ) {
        
        mInited = init();
    
}


MainControl::~MainControl() {
    
    mRenderer->release();
    
}


void MainControl::run() {
    
/*     int w = Renderer::W_WIDTH;
    int h = Renderer::W_HEIGHT;  
    newExplosion(w/2, h/2); */
                      
    unsigned int tBegin = 0;
    unsigned int tElapsed = 0;
    unsigned int tMin = 0xFFFFFFFF;
    unsigned int tMax = 0;                                                      
    
    
    unsigned int tCurrentFrame = 0;
    unsigned int tPreviousFrame = 0;
    unsigned int loops = 0;
    
    // update loop
    while ( !mInput->shouldQuit() ) {
        
        // update fps counter
        tCurrentFrame = SDL_GetTicks();
        if (loops > 30) {
            mRenderer->updateFPS(tCurrentFrame - tPreviousFrame);
            loops = 0;
        }
        tPreviousFrame = tCurrentFrame;
        
        mRenderer->clear();
        
        if ( mInput->enterPressed() ) {
            std::cout << "reset\n";
            mPopulation.reset();           
        }
        mPopulation.update(this);
        
        
        tBegin = SDL_GetTicks();
        
        handleExplosions();
        
        tElapsed = SDL_GetTicks() - tBegin;
        if (tElapsed < tMin && tElapsed != 0) tMin = tElapsed;
        if (tElapsed > tMax) tMax = tElapsed;
        
        mRenderer->render();
        
        ++loops;
        
    }
    
    std::cout << "***Explosions update time***\n"
              << "min: " << tMin << " ms\n"
              << "max: " << tMax << " ms\n";
    
    
    
}


bool MainControl::isInited() {
    return mInited;
}


bool MainControl::init() {
    
    if ( !mRenderer->isInited() ) {
        std::cerr << "initialization failed\n";
        return false;
    }
    
    
    // bg color for screen and swarm tex is the same, alpha for tex is 0
    unsigned int red   = Renderer::BG_COLOR.r << 24;
    unsigned int green = Renderer::BG_COLOR.g << 16;
    unsigned int blue  = Renderer::BG_COLOR.b << 8;
    
    mExplosionColor = red + green + blue;
    
    
    return true;
    
    
}


void MainControl::newExplosion(int x, int y) {
    int w = 2*Renderer::W_WIDTH/3;
    
    mExplosions.push_back(new Explosion( {x, y, w, w}, mExplosionColor ));
}


void MainControl::handleExplosions() {
    
    // clean
    for (auto it = mExplosions.begin(); it != mExplosions.end();) {
        
        if ( (*it)->isFinished() ) {
            it = mExplosions.erase(it);
        
        } else {
            ++it;
        }
        
    }
    
    // update
    for (unsigned int i=0; i<mExplosions.size(); ++i) {
        mExplosions[i]->update( mRenderer->getRenderer() );
    }  
    
}