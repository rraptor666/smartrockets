#ifndef POPULATION_HH
#define POPULATION_HH

#include "rocket.hh"
#include "rocketfactory.hh"
#include <SDL2/SDL.h>
#include <string>
#include <vector>

class MainControl;

class Population {
    
public:
    Population();
    ~Population();
    
    
    void update(MainControl* obj);
    void reset();
    
    /**
     * @brief evaluate, creates mating pool based on fitness values
     * @param target, target object's coordinates, used for determining fitness 
     */
    void evaluate(const SDL_Point &target);
    
    /**
     * @brief selection, breeds random parents from mating pool
     */
    void selection();
    
    unsigned int getCurrentFrame();
    
    
private:
    std::vector<Rocket*> mRockets;     // contains all rockets
    std::vector<Rocket*> mMatingPool;  // contains parents for reproduction
    
    unsigned int mCount;  // popoulation count
    SDL_Rect mTarget;     // target for rockets
    
    RocketFactory* mRocketFactory;
    
    
    static const unsigned int ROCKET_COUNT;
    static const unsigned int LIFESPAN;
    
};


#endif
