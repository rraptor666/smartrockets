#include "dna.hh"

const float Dna::MUTATION_RATE = 0.01f;
const float Dna::MAX_FORCE = 1.5f;

Dna::Dna(unsigned int amount) {
    
    create(amount);
    
}


Dna::Dna(const Dna &other):
    mGenes(other.mGenes) {
}


Dna::Dna(ForceVec &genes):
    mGenes(genes) {
}


Dna::~Dna() {
    
}


const ForceVec* Dna::getGenes() {
    return &mGenes;
}


void Dna::mutation() {
    
    for (unsigned int i=0; i<mGenes.size(); ++i) {
        
        if ( Utility::random(0, 100) < (int)(MUTATION_RATE * 100.0f) ) {
            
            float x = Utility::random(-200, 200) / 100.0f;
            float y = Utility::random(-200, 200) / 100.0f;
            
            mGenes[i] = {x, y};
        }
    }
    
}


Dna Dna::crossover(const Dna &partner) {
    
    unsigned int mid = Utility::random(0, mGenes.size()-1);
    ForceVec newGenes;
    
    for (unsigned int i=0; i<mGenes.size(); ++i) {
        
        if (i > mid) {
            newGenes.push_back( mGenes[i] );
            
        } else {
            newGenes.push_back( partner.mGenes[i] );
        }
    }
    
    return Dna(newGenes);
    
}



void Dna::create(unsigned int amount) {
    
    for (unsigned int i=0; i<amount; ++i) {
        
        float x = Utility::random(-200, 200) / 100.0f;
        float y = Utility::random(-200, 200) / 100.0f;
        
        Vec2 tmp = {x, y};
        mGenes.push_back( tmp.getNormalized() * MAX_FORCE );
    }
    
    
}


void Dna::reset() {
    
    unsigned int count = mGenes.size();
    mGenes.clear();
    create(count);
}
