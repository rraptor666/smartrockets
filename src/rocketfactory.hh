#ifndef ROCKETFACTORY_HH
#define ROCKETFACTORY_HH


#include "rocket.hh"
#include "utility.hh"

class RocketFactory {

public:
    static RocketFactory* getInstance();
    
    void initialize(const int wWidth, const int wHeight);
    
    Rocket* buildRocket(unsigned int lifespan);
    Rocket* buildRocket(unsigned int lifespan, const Dna &child);
    
    
    
private:
    RocketFactory();
    ~RocketFactory() {};
    
    
    SDL_Texture* mRocket;
    TexVec mEngineTex;
    
    SDL_Rect mRocketDim;
    SDL_Rect mENgineDim;
    SDL_Point mWindowDim;


    static const std::string ROCKET_PATH;
    static const std::vector<std::string> ENGINE_PATH;
    static const float SIZE_FACTOR;
    
};

#endif
