#ifndef VEC2_HH
#define VEC2_HH

#include <math.h>


class Vec2 {

public:
    Vec2(float x, float y):
        x(x),
        y(y) {
    }
    
    Vec2():
        x(0.0f),
        y(0.0f) {
    }

    Vec2(const Vec2& other):
        x(other.x),
        y(other.y) {
    }
    
    
    float magnitude() const {
        return sqrt(x*x + y*y);
    }
    
    
    Vec2 getNormalized() {
        
        float mag = sqrt(x*x + y*y);
        if (mag > 0.0f) {
            return {x/mag, y/mag};
            
        } else {
            return *this;
        }
        
    }
    
    void normalize() {
        
        float mag = sqrt(x*x + y*y);
        
        if (mag > 0.0f) {
            x /= mag;
            y /= mag;
        }
    }
    
    float distance(const Vec2 &other) {
        return sqrt( (x-other.x)*(x-other.x) + (y-other.y)*(y-other.y) );
    }
    
    
    /**
     * @brief vectorAngle, return angle(degrees) of two vectors
     */
    static float vectorAngle(const Vec2 &a, const Vec2 &b) {

        float mag_a = sqrt(a.x*a.x + a.y*a.y);
        float mag_b = sqrt(b.x*b.x + b.y*b.y);
        float dot = a.x*b.x + a.y*b.y;
        
        return acos( dot / (mag_a * mag_b) ) * 180.0f/M_PI;
    }
    
    
    float vectorAngle(const Vec2 &other) {

        float mag_a = sqrt(x*x + y*y);
        float mag_b = sqrt(other.x*other.x + other.y*other.y);
        float dot = x*other.x + y*other.y;
        
        return acos( dot / (mag_a * mag_b) ) * 180.0f/M_PI;
    }


    float vectorAngle2(const Vec2 &other) {
        
        float dot = x*other.x + y*other.y;
        float det = x*other.y - y*other.x;
        float unitAngle = atan2(det, dot);
        
        return acos(unitAngle) * 180.0f/M_PI;
    }
    
    static Vec2 rotateVector(const Vec2 &v, float angle) {
        
        float rads = angle * M_PI/180.0f;
        
        // float x = cos(rads) * v.x - sin(rads) * v.y;
        // float y = sin(rads) * v.x + cos(rads) * v.y;
        float mag = sqrt( v.x*v.x + v.y*v.y );
        rads -= M_PI/2.0f;
        float x = mag * cos(rads);
        float y = mag * sin(rads);
        
        return {x, y};
    }
    
    
    Vec2 operator+(const Vec2& other) {
        return Vec2(x + other.x, y + other.y);
    }
    
    Vec2 operator-(const Vec2& other) {
        return Vec2(x - other.x, y - other.y);
    }

    Vec2& operator+=(const Vec2& other) {
        x += other.x;
        y += other.y;
        
        return *this;
    }
    
    Vec2& operator-=(const Vec2& other) {
        x -= other.x;
        y -= other.y;
        
        return *this;
    }
    
    Vec2& operator+=(const float f) {
        x += f;
        y += f;
        
        return *this;
    }
    
    Vec2& operator-=(const float f) {
        x -= f;
        y -= f;
        
        return *this;
    }

    Vec2& operator=(const float f) {
        x = f;
        y = f;
        
        return *this;
    }    
    
    
    Vec2 operator*(const float f) const {
        return Vec2(x * f, y * f);
    }

    
    float x;
    float y;
};


#endif
