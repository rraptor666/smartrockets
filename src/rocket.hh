#ifndef ROCKET_HH
#define ROCKET_HH

#include "vec2.hh"
#include "engine.hh"
#include "dna.hh"
#include <random>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


class Rocket {
    
public:

    Rocket(const SDL_Rect &pos, 
           SDL_Texture* rockettex, 
           unsigned int lifespan,
           TexVec* enginetex=nullptr);
           
    Rocket(const SDL_Rect &pos, 
           SDL_Texture* rockettex, 
           unsigned int lifespan, 
           const Dna &dna,
           TexVec* enginetex=nullptr);
           
    ~Rocket();
    
    
    void update(SDL_Renderer* r);
    void reset(bool hardreset=false);
    void applyForce(const float f);
    void applyForce(const Vec2 &v);
    
    const SDL_Rect* getPos();
    const SDL_Point getTipPos();
    bool isDestroyed();
    bool crash();
    
    // dna functions
    float getFitness(const SDL_Point &target);
    void normalizeFitness(float maxFit);
    Dna* getDna();
    



private:
    void move();

    SDL_Rect mPos;
    SDL_Point mStartPos;
    
    SDL_Texture* mTex;
    SDL_Texture* mEngineTex;
    
    SDL_Rect mEnginePos;
    SDL_Rect mEngineStartPos;
    Engine mEngine;
    
    Vec2 mAccum;
    
    SDL_Rect mTipPos;
    
    unsigned int mFrame;
    unsigned int mMaxFrames;
    bool mDestroyed;
    bool mFinished;
    
    Dna mDna;
    float mFitness;
    
    Vec2 mVel;
    Vec2 mAccel;
    float mAngle;
    
    
    static const Vec2 UP;
    static const float FORCE_FACTOR;
    
    
    
};


#endif
