#include "renderer.hh"
#include <iostream>

const int Renderer::W_WIDTH = 800;
const int Renderer::W_HEIGHT = 600;

const SDL_Color Renderer::WHITE = {255, 255, 255, 255};
const SDL_Color Renderer::BLACK = {0, 0, 0, 255};
const SDL_Color Renderer::GREY = {90, 90, 90, 255};
const SDL_Color Renderer::BG_COLOR = BLACK;

Renderer* Renderer::getInstance() {
    static Renderer instance;
    return &instance;
}
    
    
Renderer::Renderer():
    mTitle("SmartRockets"),
    mRenderer(nullptr),
    mWindow(nullptr) {
        
        init();
        
}


void Renderer::release() {
    SDL_DestroyWindow(mWindow);
    SDL_DestroyRenderer(mRenderer);
    
    IMG_Quit();
    SDL_Quit();   
}


void Renderer::init() {
    
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cerr << "Renderer: " << SDL_GetError() << "\n";
        mInited = false;
        return;
    }
    
    mWindow = SDL_CreateWindow(mTitle.c_str(),
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               W_WIDTH,
                               W_HEIGHT,
                               0);
                               
    if (mWindow == nullptr) {
        std::cerr << "Renderer: " << SDL_GetError() << "\n";
        mInited = false;
        return;
    }                
    
    mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_PRESENTVSYNC);           
    
    if (mRenderer == nullptr) {
        std::cerr << "Renderer: " << SDL_GetError() << "\n";
        mInited = false;
        return;
    }
    
    
    mInited = true;    
}


void Renderer::render() {
    SDL_RenderPresent(mRenderer);
}


void Renderer::clear() {
    SDL_SetRenderDrawColor(mRenderer, BG_COLOR.r, BG_COLOR.g, BG_COLOR.b, BG_COLOR.a);
    SDL_RenderClear(mRenderer);
}


bool Renderer::isInited() {
    return mInited;
}


SDL_Renderer* Renderer::getRenderer() {
    return mRenderer;
}


void Renderer::setTitle(const std::string &str) {
    SDL_SetWindowTitle( mWindow, str.c_str() );
}


std::string Renderer::getTitle() {
    return mTitle;
}


void Renderer::updateFPS(const unsigned int& t_delta) {
    std::string fps = std::to_string( 1000 / t_delta );
    std::string title = mTitle + " | " + std::string( 4-fps.size(), ' ' ) + fps;
    SDL_SetWindowTitle(mWindow, title.c_str());
}