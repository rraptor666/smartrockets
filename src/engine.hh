#ifndef ENGINE_HH
#define ENGINE_HH

#include "vec2.hh"
#include <SDL2/SDL.h>
#include <vector>


using TexVec = std::vector<SDL_Texture*>;

/**
 * @brief Engine, creates rocket propulsion animation
 *        and updates it
 */
class Engine {

public:
    Engine(TexVec* tex);
    ~Engine();
    
    /**
     * @brief update, move and draw rocket propulsion
     * @param dim, dimensions for animation textures 
     * @param angle, texture rotation angle, 0.0f causes propulsion to point down (+y) 
     */
    void update(SDL_Renderer* r, 
                const SDL_Point& center, 
                const SDL_Rect &pos, 
                float angle=0);


private:
    SDL_Rect mDim;
    TexVec* mTextures;    
    unsigned int mFrame;
    
    float mAngle;


};


#endif