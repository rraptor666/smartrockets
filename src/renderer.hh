#ifndef RENDERER_HH
#define RENDERER_HH

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>


class Renderer {
    
public:
    static Renderer* getInstance();
    void release();
    
    void render();
    void clear();
    
    bool isInited();
    SDL_Renderer* getRenderer();
    
    void setTitle(const std::string &str);
    std::string getTitle();
    
    void updateFPS(const unsigned int& t_delta);
    
    
    static const int W_WIDTH;
    static const int W_HEIGHT;
    static const SDL_Color BG_COLOR;

private:
    Renderer();
    ~Renderer() {};
    void init();
    
    bool mInited;
    std::string mTitle;
    
    SDL_Renderer* mRenderer;
    SDL_Window* mWindow;
    
    
    static const SDL_Color WHITE;
    static const SDL_Color BLACK;
    static const SDL_Color GREY;
    
    
};



#endif
