#include "rocketfactory.hh"
#include "renderer.hh"

const std::string RocketFactory::ROCKET_PATH = "res/rocket2.png";
const float RocketFactory::SIZE_FACTOR = 30.0f / 146.0f; // 146x235 px

const std::vector<std::string> RocketFactory::ENGINE_PATH = {
    
    // "res/exhaust2/2.png",
    // "res/exhaust2/3.png",
    // "res/exhaust2/3.png",
    // "res/exhaust2/6.png",
    // "res/exhaust2/3.png", 

    "res/exhaust/0.png",
    "res/exhaust/1.png",
    "res/exhaust/2.png",
    "res/exhaust/3.png",
    "res/exhaust/4.png",
    "res/exhaust/5.png",
    "res/exhaust/6.png"

};





RocketFactory* RocketFactory::getInstance() {
 
    static RocketFactory instance;
    return &instance;
}


RocketFactory::RocketFactory() {
    
}


void RocketFactory::initialize(const int wWidth, const int wHeight) {
    
    SDL_Renderer* r = Renderer::getInstance()->getRenderer();
    
    mRocket = Utility::loadTexture(r, ROCKET_PATH);
    
    int w, h;
    SDL_QueryTexture(mRocket, nullptr, nullptr, &w, &h);
    w *= SIZE_FACTOR;
    h *= SIZE_FACTOR;
    
    mRocketDim = {wWidth/2 - w/2, wHeight - 40, w, h};
    
    
    for (unsigned int i=0; i<ENGINE_PATH.size(); ++i) {
        mEngineTex.push_back( Utility::loadTexture(r, ENGINE_PATH[i]) );
    }
    
    
    
    
}


Rocket* RocketFactory::buildRocket(unsigned int lifespan) {
    
    return new Rocket(mRocketDim, mRocket, lifespan, &mEngineTex);
}

Rocket* RocketFactory::buildRocket(unsigned int lifespan, const Dna &child) {
    
    return new Rocket(mRocketDim, mRocket, lifespan, child, &mEngineTex);
}

    
    
    