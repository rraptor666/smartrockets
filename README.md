# Smart Rockets

A c++ clone of Daniel Shiffman's [Smart Rockets in p5.js](https://www.youtube.com/watch?v=bGz7mv2vD6g)


### ToDo


- [] STAND_ALONE_PROJECT: render high quality explosion into textures -> save as png to create precalculated


- [ ] add deceleration force which slows particles down ("drag") NOTE: no decel -> less particles to render
- [ ] Explosion: amount of particles depends on given canvas tex dimensions?  ~4000 for 800x800

- [ ] rocket leave trail (drawLines) (add alpha)

- [ ] smoothen movement (original smart rockets made with flash):
    * chooses force vec from predefined list (e.g. (inter)cardinal directions)
    * more tighter turns if speed is low(physics momentum formulas?) 
    
- [ ] class Obstacle/Barrier, store in vector and iter through them to check for collision

- [ ] check ToDos in code


### Time statistics

#### Swarm

##### Init

| Particles | Min Time (ms) | Max Time (ms) |
| ---------:|:-------------:|:-------------:|
|      400  |       11      |       12      |    
|     4000  |       12      |       14      |    
|    40000  |       16      |       19      |    
|  4000000  |      490      |      500      |    


##### Update

###### no blur
| Particles | Min Time (ms) | Max Time (ms) |
| ---------:|:-------------:|:-------------:|
|      400  |        9      |       33      |    
|     4000  |        8      |       20      |    
|    40000  |        7      |       34      |    
|  4000000  |       44      |      163      |    
<br/>


###### with simple box blur
| Particles | Min Time (ms) | Max Time (ms) |
| ---------:|:-------------:|:-------------:|
|      400  |        0      |        0      |    
|     4000  |       48      |       83      |    
|    40000  |        0      |        0      |    
|  4000000  |        0      |        0      | 


### References

fastBoxBlur algorithm after Kutskir, I., fastest gaussian blur, http://blog.ivank.net/fastest-gaussian-blur.html

simpleBoxBlur algorithm after Purcell, J., box blur, https://www.youtube.com/watch?v=f84WCQHTTPA