OBJS = src/*.cc

CC = g++

COMPILER_FLAGS = -std=c++17 -Wall

LINKER_FLAGS = -lSDL2 -lSDL2_image


OBJ_NAME = rockets
DB_NAME = debug

.PHONY: all debug


all : $(OBJS) ; $(CC) $(COMPILER_FLAGS) $(OBJS) $(LINKER_FLAGS) -o $(OBJ_NAME)
debug : $(OBJS) ; $(CC) $(COMPILER_FLAGS) -g $(OBJS) $(LINKER_FLAGS) -o $(DB_NAME)